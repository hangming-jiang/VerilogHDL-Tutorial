//两级流水实现的8位加法器
module adder_pipe2(cout,sum,ina,inb,cin,clk);
input[7:0] ina,inb; input cin,clk; output reg[7:0] sum;
output reg cout; reg[3:0] tempa,tempb,firsts; reg firstc;
always @(posedge clk)
begin  {firstc,firsts}=ina[3:0]+inb[3:0]+cin;
    tempa=ina[7:4];  tempb=inb[7:4];
end
always @(posedge clk)
begin  {cout,sum[7:4]}=tempa+tempb+firstc;
    sum[3:0]=firsts;
end
endmodule
