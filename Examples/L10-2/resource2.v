// 2个选择器和1个加法器的实现方式
module resource2(sum,a,b,c,d,sel);
parameter SIZE=4; input sel; input[SIZE-1:0] a,b,c,d;
output reg[SIZE:0] sum; reg[SIZE-1:0] atemp,btemp;
always @(*)           //使用通配符
begin if(sel) begin atemp=a;btemp=b;end
    else  begin atemp=c;btemp=d;end
    sum=atemp+btemp;
end
endmodule
