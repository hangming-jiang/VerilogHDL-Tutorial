//阻塞赋值方式描述的移位寄存器3。
module block3(q0,q1,q2,q3,din,clk);
input clk,din; output reg q0,q1,q2,q3;
always @(posedge clk)
begin 	q0=din;
    //4条赋值语句的顺序与例10.9中完全颠倒
    q1=q0;
    q2=q1;
    q3=q2;
end
endmodule
