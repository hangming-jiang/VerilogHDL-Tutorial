//非阻塞赋值方式描述的移位寄存器。
module block4(q0,q1,q2,q3,din,clk);
input clk,din; output reg q0,q1,q2,q3;
always @(posedge clk)
begin 	q3<=q2;
    q1<=q0;
    q2<=q1;
    q0<=din;
end  endmodule
