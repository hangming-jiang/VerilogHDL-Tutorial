//长帧同步时钟的产生
module longframe1(clk,strb);
parameter DELAY=8; input clk;
output reg strb; reg[7:0] counter;
always@(posedge clk)
begin if(counter==255) counter<=0;
    else counter<=counter+1;
end
always@(counter)
begin if(counter<=(DELAY-1)) strb<=1; else strb<=0; end
endmodule
