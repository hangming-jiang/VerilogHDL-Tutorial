//被测试模块
module mult8(out,a,b);//8位乘法器源代码
parameter size=8;
input[size:1] a,b;	 //两个操作数
output[2*size:1] out;//结果
assign out=a*b;       //乘法运算符
endmodule
