//8位计数器的仿真代码
`timescale 10ns/1ns
module count8_tp;
reg clk,reset;		//输入激励信号定义为reg型
wire[7:0] qout;       		//输出信号定义为wire型
parameter DELY=100;
counter C1(qout,reset,clk);  	//调用测试对象
always #(DELY/2) clk=~clk;	//产生时钟波形
initial begin 		//激励波形定义
    clk=0; reset=0;
    #DELY  	reset=1;
    #DELY  	reset=0;
    #(DELY*300) $finish;
end
initial $monitor($time,,,"clk=%d reset=%d qout=%d",clk,reset,qout);
endmodule
