//n为5反馈系数Ci分别为(45)8和(57)8的Gold码序列发生器
module gold(clr,clk,gold_out);
input clr,clk; output gold_out;
reg[4:0] shift_reg1,shift_reg2;
assign gold_out=shift_reg1[4] ^ shift_reg2[4];  //两个m序列异或
always @(posedge clk or negedge clr)
begin  if(~clr) begin
        shift_reg1<=5'b00001;
        shift_reg2<=5'b00001; end 		//异步复位
    else  begin
        shift_reg1[0]<=shift_reg1[2] ^ shift_reg1[4];
        //反馈系数Ci为(45)8
        shift_reg1[4:1]<=shift_reg1[3:0];
        shift_reg2[0]<=shift_reg2[1] ^ shift_reg2[2] ^
                  shift_reg2[3] ^ shift_reg2[4];
        //反馈系数Ci为(57)8
        shift_reg2[4:1]<=shift_reg2[3:0];
    end
end
endmodule
