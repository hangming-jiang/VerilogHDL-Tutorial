//4位二进制加法器的Verilog描述
module add4_bin(cout,sum,ina,inb,cin);
input cin; input[3:0] ina,inb;
output[3:0] sum; output cout;
assign {cout,sum} = ina+inb+cin;/*逻辑功能定义*/
endmodule
