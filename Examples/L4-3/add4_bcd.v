//BCD码加法器
module add4_bcd(cout,sum,ina,inb,cin);
input cin; input[3:0] ina,inb;
output[3:0] sum; reg[3:0] sum;
output cout; reg cout;
reg[4:0] temp;
always @(ina,inb,cin) 	//always过程语句
begin  temp<=ina+inb+cin;
    if(temp>9) {cout,sum}<=temp+6;
    //两重选择的IF语句
    else {cout,sum}<=temp;
end
endmodule
