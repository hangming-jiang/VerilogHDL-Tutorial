//带异步清0/异步置1（低电平有效）的D触发器
module dff_asyn(q,qn,d,clk,set,reset);
input d,clk,set,reset; output reg q,qn;
always @(posedge clk or negedge set or negedge reset)
begin
    if(~reset) 	begin q<=1'b0;qn<=1'b1; end
    //异步清0，低电平有效
    else if(~set) begin q<=1'b1;qn<=1'b0; end
    //异步置1，低电平有效
    else  	begin  q<=d;qn<=~d; end
end
endmodule
