//用文本方式调用RAM存储器模块
module ram_dy(addr,clk,din,wr,qout);   //端口定义
input[6:0] addr;
input clk;
input[7:0] din;
input wr;
output[7:0] qout;
myram u1(address(addr),.clock(clk),.data(din),.wren(wr),.q(qout));	//元件例化
endmodule
