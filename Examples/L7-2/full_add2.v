//数据流描述的1位全加器
module full_add2(a,b,cin,sum,cout);
input a, b, cin;
output sum, cout;
assign  sum = a ^ b ^ cin;
assign  cout = (a & b ) | (b & cin ) | (cin & a );
endmodule
