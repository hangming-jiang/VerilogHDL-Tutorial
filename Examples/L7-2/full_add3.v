//行为描述的1位全加器
module full_add3(a,b,cin,sum,cout);
input a,b,cin; output reg sum,cout;
always @*
    //或写为always @(a or b or cin)
begin
    {cout,sum}=a+b+cin;
end
endmodule
