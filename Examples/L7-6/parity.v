//奇偶校验位产生器
module parity(even_bit,odd_bit,a);
input[7:0] a;
output even_bit,odd_bit;
assign even_bit=^a;
//生成偶校验位
assign odd_bit=~even_bit;
//生成奇校验位
endmodule
