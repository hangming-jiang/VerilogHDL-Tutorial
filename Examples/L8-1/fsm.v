//用状态机设计模5计数器
module fsm(clk,clr,z,qout);
input clk,clr;
output reg z;
output reg[2:0] qout;
always @(posedge clk or posedge clr) 		//此过程定义状态转换
begin
    if(clr)
        qout<=0;              		//异步复位
    else
    case(qout)
        3'b000:
            qout<=3'b001;
        3'b001:
            qout<=3'b010;
        3'b010:
            qout<=3'b011;
        3'b011:
            qout<=3'b100;
        3'b100:
            qout<=3'b000;
        default:
            qout<=3'b000;	/*default语句*/
    endcase
end
always @(qout) 				/*此过程产生输出逻辑*/
begin
    case(qout)
        3'b100:
            z=1'b1;
        default:
            z=1'b0;
    endcase
end
endmodule
