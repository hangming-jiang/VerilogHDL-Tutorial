//“101”序列检测器的Verilog描述（三个过程）
module fsm1_seq101(clk,clr,x,z);
input clk,clr,x;
output reg z;
reg[1:0] state,next_state;
parameter 	S0=2'b00,S1=2'b01,S2=2'b11,S3=2'b10;
/*状态编码，采用格雷（Gray）编码方式*/
always @(posedge clk or posedge clr) /*该过程定义当前状态*/
begin
    if(clr) state<=S0;        //异步复位，s0为起始状态
    else    state<=next_state;
end
always @(state or x) 		/*该过程定义次态*/
begin
    case (state)
        S0: begin
            if(x) next_state<=S1;
            else  next_state<=S0;
        end
        S1: begin
            if(x) next_state<=S1;
            else  next_state<=S2;
        end
        S2: begin
            if(x) next_state<=S3;
            else  next_state<=S0;
        end
        S3: begin
            if(x) next_state<=S1;
            else  next_state<=S2;
        end
        default:next_state<=S0;
         	/*default语句*/
    endcase
end
always @(state) 			/*该过程产生输出逻辑*/
begin
    case(state)
        S3:  z=1'b1;
        default: z=1'b0;
    endcase
end
endmodule
