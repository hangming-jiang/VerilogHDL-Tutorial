//数据流描述的加法器
module add_bx(cout,sum,a,b,cin);
parameter WIDTH=8;
input cin;
output cout;
input[WIDTH-1:0] a,b;
output[WIDTH-1:0] sum;
assign {cout,sum}=a+b+cin;
endmodule
