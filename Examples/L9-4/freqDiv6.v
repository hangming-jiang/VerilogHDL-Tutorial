//偶数分频实例（6分频）
module freqDiv6(clkin,clkout);
input clkin;
output reg clkout;
reg[2:0] cnt;

always @(posedge clkin)
begin
    cnt = cnt+1;
    if (cnt>=3)
    begin
        cnt = 0;
        clkout = ~clkout;
    end
end
endmodule
