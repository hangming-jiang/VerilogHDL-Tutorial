//利用移位相加实现乘法器
module multiplier8Bits(clk,rst,a,b,R);
input clk,rst;
input[7:0] a,b;
output reg[15:0] R;

reg[15:0] temp_R,temp_a;
reg[7:0] temp_b;

integer i;

always @(posedge clk)
	begin
		if (rst) begin temp_R=0;temp_a=a;temp_b=b; end
		else
			begin
				temp_R=0;temp_a=a;temp_b=b;
				for (i=0;i<8;i=i+1)
					begin
						if(temp_b[0]) begin temp_R=temp_R+temp_a; end
						else begin temp_R=temp_R; end
						temp_a=temp_a<<1;
						temp_b=temp_b>>1;
					end
				R=temp_R;	
			end
	end


endmodule
