module LedTrans(clk,reset,segData,sel,led);
input clk,reset;//
output[6:0] segData;
output[1:0] sel;
output[15:0] led;

wire clk1,clk2,done;
wire[6:0] setVal,cntVal;

//freqDiv(clkin,reset,clkout1,clkout2)
freqDiv myfrqDiv(clk,reset,clk1,clk2);

//counter(reset,clk,setVal,cntVal,done);
counter myCnt(reset,clk1,setVal,cntVal,done);

//display(reset,clk,dispVal,segData,sel);
display myDisplay(reset, clk2, cntVal, segData, sel);

//transFM(reset,switch, cntVal, ledLight);
transFM myFM(reset, done, setVal, led);

endmodule
