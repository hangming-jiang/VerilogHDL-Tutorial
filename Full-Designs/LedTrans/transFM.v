module transFM(reset,switch, cntVal, ledLight);
parameter S1=16'h2828, S2=16'h1818, S3=16'h4848, S4=16'h8282, S5=16'h8181, S6=16'h8484;
input reset,switch;
output reg[6:0] cntVal;
output reg[15:0] ledLight;

reg[15:0] state;

always @ (posedge switch or posedge reset)
	begin
		if (reset)
			state <= S1;
		else
			case (state)
				S1:state <= S2;
				S2:state <= S3;
				S3:state <= S4;
				S4:state <= S5;
				S5:state <= S6;
				S6:state <= S1;
				default:state <= S1;
			endcase		
	end
always @ (state)
	begin
		ledLight<=state;
		case (state)
			S1:cntVal<=60;
			S2:cntVal<=30;
			S3:cntVal<=10;
			S4:cntVal<=60;
			S5:cntVal<=30;
			S6:cntVal<=10;
			default:cntVal<=60;
		endcase	
	end
endmodule
