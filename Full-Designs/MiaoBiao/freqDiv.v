
module freqDiv(clk,clk100Hz,clk150Hz);

input clk;//50MHz
output reg clk100Hz,clk150Hz;

reg[19:0] cnt100Hz,cnt150Hz;//111 1010 0001 0010 0000

always @(posedge clk)//100Hz Div
	begin
	  	cnt100Hz = cnt100Hz+1;
		if (cnt100Hz >500000/*50*/) begin cnt100Hz=0; clk100Hz=1; end
		else clk100Hz=0;
	end
	
always @(posedge clk)//150Hz Div
	begin
	  	cnt150Hz = cnt150Hz+1;
		if (cnt150Hz >333333/*33*/) begin cnt150Hz=0; clk150Hz=1; end
		else clk150Hz=0;
	end	

endmodule
