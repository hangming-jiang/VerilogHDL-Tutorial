//1、实现一个八位乘法器
module mult_8Bits(rst,clk,a,b,outcome);
parameter SIZE=8;
input rst,clk;
input[SIZE:1] a,b;
output reg[2*SIZE:1]  outcome;
reg[2*SIZE:1]  temp_a;
reg[SIZE:1]  temp_b;
integer i;

always @(posedge clk)
begin
    if (rst) outcome=0;
    else
        begin
            outcome=0; temp_a=a;  temp_b=b;
            for (i=1;i<=SIZE;i=i+1)
            begin
                if(temp_b[1])  outcome=outcome +temp_a; 	//如果temp_b的最低位为1，就执行下面的加法
                temp_a=temp_a<<1;    //操作数a左移一位
                temp_b=temp_b>>1;    //操作数b右移一位
            end
        end
end 
endmodule
